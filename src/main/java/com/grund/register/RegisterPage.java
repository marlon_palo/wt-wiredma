package com.grund.register;

import org.openqa.selenium.WebDriver;

import com.grund.verify.verifyXPath;

public class RegisterPage {

	private static final String LBL_PHPFATALERROR_XPATH = "//b[text()='Fatal error']";
	
	public static boolean noPhpErrorFound(WebDriver driver) {
		int xpathctr = verifyXPath.count(driver, LBL_PHPFATALERROR_XPATH,"REGISTER: Verify No PHP Error on Page.");
		
		if(xpathctr < 0){
			return true;
		} else {
			return false;
		}
		
	}

}
