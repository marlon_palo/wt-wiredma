package com.grund.header;

import org.openqa.selenium.WebDriver;

import com.grund.request.ClickElement;

public class Header {

	private static final String LINK_REGISTER_XPATH = "//a[@href='/?page=register_form']";

	public static void clickRegister(WebDriver driver) {
		ClickElement.byXPath(driver, LINK_REGISTER_XPATH, "HEADER: Click Register Link.");
		
	}

}
