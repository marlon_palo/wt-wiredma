package com.grund.includes;

import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.grund.utility.StatusLog;
import com.grund.utility.TableContainer;
import com.grund.utility.TakeScreenShot;

public class Quit {

	static Properties sys = System.getProperties();
	
	public static void endTest(WebDriver driver, String reportDIR, String cellValue, String colName, String result) {
		
		StatusLog.log("Ending test run...");
		TableContainer.insertIntoCellbyCellValueColValue(reportDIR, cellValue, colName, result);
		
		try {
			TakeScreenShot.CurrentPage(driver, "Last Page Results");
		} catch (IOException e) {
			StatusLog.log("[SCREENSHOT] Error on getting screenshot.");
		}
		
		driver.close();
		driver.quit();
		
	}

}
