package com.grund.includes;

import org.openqa.selenium.WebDriver;

import com.grund.register.RegisterPage;

public class RegisterForm {

	

	public static void navigateForm(WebDriver driver) {
		HeaderNav.clickRegister(driver);
		
	}

	public static boolean noErrorPageFound(WebDriver driver) {
		
		return RegisterPage.noPhpErrorFound(driver);
		
		
	}

}
