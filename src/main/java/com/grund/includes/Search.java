package com.grund.includes;

import org.openqa.selenium.WebDriver;

import com.grund.yelu.Srch.Srch;

public class Search {

	public static void submitSearchQuery(WebDriver driver, String keyword,
			String location) {
		Srch.setSearchField(driver, keyword);
		Srch.setSearchLocation(driver, location);
		Srch.submitSearch(driver);
		
	}

}
