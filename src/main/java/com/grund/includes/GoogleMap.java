package com.grund.includes;

import org.openqa.selenium.WebDriver;

import com.grund.googlemaps.googleMaps;
import com.grund.utility.TableContainer;

public class GoogleMap {


	public static void searchKeyword(WebDriver driver, String keyword) {
		googleMaps.setSearchKeyword(driver, keyword);
		googleMaps.clickSearchBtn(driver);
		
	}

	public static void storeAllURLinPageResults(WebDriver driver,
			String excelFileDir) throws Exception {
		
		int resultCtr = googleMaps.getItemPageResultsCount(driver);
				
		String CompanyName;
		int RowLastNum;
		
		System.out.println(resultCtr);
		
		//get the last row count first
		RowLastNum = TableContainer.getRowCountinExcelFile(excelFileDir);
		
		for(int n = 1; n <= resultCtr; n++){
			
			try {
				
				
				CompanyName = googleMaps.getResultTitlebyPosition(driver, n);
				
				googleMaps.clickItemResultsbyPosition(driver, n);
					
				String URl = googleMaps.getItemDetailURLText(driver);
				
				//insert after the last row
				int rowIndex = RowLastNum + n;

				TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "companyName",CompanyName);
				TableContainer.insertIntoCellValue(excelFileDir, rowIndex, "url",URl);
				
				googleMaps.clickBacktoResults(driver);
			
				
				
			} catch (Throwable e){
				
			}
			
			
		} //end for
		
	}

	public static Boolean isNextPageActive(WebDriver driver) throws Exception {
		
		return googleMaps.isNextPageActive(driver);
		
	}

	public static void clickNextPageResults(WebDriver driver) {
		googleMaps.clickNextPageResults(driver);
		
	}

}
