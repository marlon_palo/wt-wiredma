package com.grund.yelu.Srch;

import org.openqa.selenium.WebDriver;

import com.grund.form.SetInputField;
import com.grund.request.ClickElement;

public class Srch {

	private static final String INPUT_SEARCH_XPATH = "//input[@id='CompanySearchQuery']";
	private static final String INPUT_LOCATION_XPATH = "//input[@id='CompanySearchLocation']";
	private static final String LINK_SEARCHSUBMIT_XPATH = "//a[contains(@onclick,'search_form')]";

	public static void setSearchField(WebDriver driver, String keyword) {
		SetInputField.byXPath(driver, INPUT_SEARCH_XPATH, keyword, "SEARCH: Enter Companies or Services.");
		
	}

	public static void setSearchLocation(WebDriver driver, String location) {
		SetInputField.byXPath(driver, INPUT_LOCATION_XPATH, location, "SEARCH: Enter Location.");
		
	}

	public static void submitSearch(WebDriver driver) {
		ClickElement.byXPath(driver, LINK_SEARCHSUBMIT_XPATH, "SEARCH: Submit Query.");
		
	}

}
