package com.grund.googlemaps;

import org.openqa.selenium.WebDriver;

import com.grund.form.SetInputField;
import com.grund.request.ClickElement;
import com.grund.utility.StatusLog;
import com.grund.verify.verifyXPath;

public class googleMaps {

	private static final String INPUT_SEARCHFIELD_XPATH = "//input[@id='searchboxinput']";
	private static final String BTN_SEARCHGO_XPATH = "//button[@class='searchbox-searchbutton']";
	private static final String DIV_ITEMRESULTS_XPATH = "//div//h3[@class='widget-pane-section-result-title']";
	private static final String LINK_ITEMDETAILURLINFO_XPATH = "//div[@class='widget-pane-section-info-line']//a[@data-attribution-url]";
	private static final String LINK_ITEMDETAILBACKTORESULTS_XPATH = "//span[text()='Back to results']";
	private static final String BTN_NEXTPAGE_XPATH = "//a[@class='cards-categorical-pagination-button']//span[@class='cards-categorical-pagination-button-right']";
	
	public static void setSearchKeyword(WebDriver driver, String keyword) {
		SetInputField.byXPath(driver, INPUT_SEARCHFIELD_XPATH, keyword, "GOOGLE MAP: Enter keyword: " + keyword);
		
	}

	public static void clickSearchBtn(WebDriver driver) {
		ClickElement.byXPath(driver, BTN_SEARCHGO_XPATH, "GOOGLE MAP: Click Search button");
		
	}

	public static int getItemPageResultsCount(WebDriver driver) throws Exception {
		return verifyXPath.count(driver, DIV_ITEMRESULTS_XPATH, "GOOGLE MAP:Get the number of items return by search.");
	}

	public static String getResultTitlebyPosition(WebDriver driver, int position) {
		return verifyXPath.getText(driver, "(" + DIV_ITEMRESULTS_XPATH + ") [position()=" + position +"]");
	}

	public static void clickItemResultsbyPosition(WebDriver driver, int position) {
		ClickElement.byXPath(driver, "(" + DIV_ITEMRESULTS_XPATH + ") [position()=" + position +"]", "GOOGLE MAP: Click the Item result in position: " + position);
		
	}

	public static String getItemDetailURLText(WebDriver driver) throws Exception {
		
		int xpathctr = verifyXPath.count(driver, LINK_ITEMDETAILURLINFO_XPATH);
		
		if(xpathctr > 0) {
			return verifyXPath.getText(driver, LINK_ITEMDETAILURLINFO_XPATH);
		} else {
			StatusLog.log("GOOGLE MAP: No Company URL found.");
			return "No URL Found";
		}
		
	}

	public static void clickBacktoResults(WebDriver driver) {
		ClickElement.byXPath(driver, LINK_ITEMDETAILBACKTORESULTS_XPATH, "GOOGLE MAPS: Click Back to Results link.");
		
	}

	public static Boolean isNextPageActive(WebDriver driver) throws Exception {
		int xpathctr = verifyXPath.count(driver, BTN_NEXTPAGE_XPATH);
		
		if(xpathctr > 0){
			return true;
		} else {
			return false;
		}
		
	}

	public static void clickNextPageResults(WebDriver driver) {
		ClickElement.byXPath(driver, BTN_NEXTPAGE_XPATH, "GOOGLE MAP: Click Next result page button.");
		
	}

}
