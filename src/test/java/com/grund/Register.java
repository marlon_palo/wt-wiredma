package com.grund;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import com.grund.engine.Config;
import com.grund.includes.Homepage;
import com.grund.includes.Quit;
import com.grund.includes.RegisterForm;
import com.grund.utility.StatusLog;
import com.grund.utility.TableContainer;



public class Register {

	public String env;
	public String keyword;
	public static String title;
	public Boolean NextPageActive;
	private static String reportDIR;
	public boolean testStatus;
	
	
	static Properties sys = System.getProperties();
	
	
	@Test
	public void REGISTER_Register_User() throws Exception{

		try 
		{
			
			//open the url
			Homepage.setupConfig(sys.getProperty("host"),sys.getProperty("browser"));
			reportDIR = sys.getProperty("tableOutPutContainer");
			Properties pr = Config.properties("config.properties"); 
			String now = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
			
			int rowCtr = TableContainer.getRowCount();
			
			for(int i = 1; i <= rowCtr; i++){
				String type = TableContainer.getCellValue(i, "type");
				String title = TableContainer.getCellValue(i, "title");
				
				RegisterForm.navigateForm(Config.driver);
				
				testStatus = RegisterForm.noErrorPageFound(Config.driver);
				StatusLog.printlnPassedResultTrue(Config.driver, "REGISTER: " + title, testStatus);

			} //end for	

			//Overall Test Result
			Assert.assertTrue(StatusLog.errMsg, StatusLog.tcStatus);
			
		}
		
			catch (Exception e)
			{
				StatusLog.log("[ERROR] Test has encountered error.");
				
			}
		
	}
	
	@AfterClass
	public static void quit() throws IOException{
		Quit.endTest(Config.driver,reportDIR, title, "Status", StatusLog.tcResult);
	}
	
	
}
