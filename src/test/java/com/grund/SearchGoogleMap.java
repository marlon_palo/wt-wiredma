package com.grund;


import java.io.IOException;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import com.grund.engine.Config;
import com.grund.includes.GoogleMap;
import com.grund.includes.Homepage;
import com.grund.utility.StatusLog;
import com.grund.utility.TableContainer;
import com.grund.utility.TakeScreenShot;


public class SearchGoogleMap {

	public String env;
	public String keyword;
	public String title;
	public Boolean NextPageActive;
	private String reportDIR;
	public boolean testStatus;
	
	
	static Properties sys = System.getProperties();
	
	
	@Test
	public void Search_GoogleMap() throws Exception{

		try 
		{
			
			//open the url
			Homepage.setupConfig(sys.getProperty("host"),sys.getProperty("browser"));
			reportDIR = sys.getProperty("tableOutPutContainer");
			
			
			//Long wait = Long.parseLong(pr.getProperty("WAIT_SEC"));
			//Get the data in the excel and quick add the skus
			int rowCtr = TableContainer.getRowCount();
			
			for(int i = 1; i <= rowCtr; i++){
	
				Config.driver.navigate().to(sys.getProperty("host"));
				
				keyword = TableContainer.getCellValue(i, "keyword");
				title = TableContainer.getCellValue(i, "title");
				
				GoogleMap.searchKeyword(Config.driver,keyword);
				
				//Look until next page is no longer active.
				do {
					GoogleMap.storeAllURLinPageResults(Config.driver, reportDIR);
					NextPageActive = GoogleMap.isNextPageActive(Config.driver);
					
					if(NextPageActive){
						GoogleMap.clickNextPageResults(Config.driver);
					}
					
				} while (NextPageActive);

			} //end for	

			//Overall Test Result
			Assert.assertTrue(StatusLog.errMsg, StatusLog.tcStatus);
			
		}
		
			catch (Exception e)
			{
				Assert.fail(e.getMessage());
				
			}
		
	}
	
	@AfterClass
	public static void quit() throws IOException{
		TakeScreenShot.CurrentPage(Config.driver, "Last Page Test Result");
		Config.driver.close();
		Config.driver.quit();
	}
	
	
}
